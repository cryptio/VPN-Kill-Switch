# VPN Kill Switch
A simple VPN kill switch written in Bash

## Installation
The scripts need executable permissions, so do this:

`sudo chmod +x on.sh off.sh`

## Usage
This kill switch is very simple to use. First, connect to your VPN. It doesn't matter which protocol is used (i.e OpenVPN, IPSec, etc). As long as a tunnel is set up, it should work.

Next, turn on your kill switch:

`./on.sh`

Now, no traffic will be allowed to travel over any interface unless it is the VPN's interface.


When you have disconnected from the VPN, be sure to turn off the kill switch:

`./off.sh`


It will reset everything and allow you to communicate over your wireless/wired interfaces again.

### OpenVPN automation
If you aren't using update-resolv-conf for up/down, you can go into your .ovpn file and add these lines:

`script-security 2`

`up /path/to/your/script/on.sh`


When the connection gets setup, it will run the on script. Obviously you don't want to set `down` to the off script, because as soon as the VPN disconnects (whether from failure or intentionally) it's going to automatically bring your other interfaces back up, thus defeating the purpose.

If you do use update-resolv-conf, you can append `up /path/to/your/script/on.sh` to the update-resolv-conf script where it says:

`case $script_type in`

   `up)`

Don't add it it to the `down` case statement, because you don't want the script reconfiguring the firewall when you lose connection.
