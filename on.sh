#!/bin/bash

echo "Current IP address:" $(curl ipinfo.io/ip)

iface=$(ip tuntap show | grep -oP "(tun[0-9])") # view the current tunnel interfaces and get the first match

if [ -z $iface ] # output is empty
then
	echo "VPN not connected"
	exit 1
fi

echo "VPN connected"

sudo ufw --force reset # reset firewall
# set rules to defaults and deny any traffic
sudo ufw default deny incoming
sudo ufw default deny outgoing
sudo ufw allow out on $iface from any to any # allow traffic from only the VPN's tunnel
sudo ufw --force enable # enable the firewall
