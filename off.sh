#!/bin/bash

echo "Turning off VPN kill switch"

sudo ufw --force reset # reset the firewall
# set default policies
sudo ufw default deny incoming
sudo ufw default allow outgoing
sudo ufw --force enable # enable the firewall
